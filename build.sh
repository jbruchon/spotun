#!/bin/bash

[ ! -z "$WINDIR" ] && LDFLAGS=-lws2_32

CFLAGS="-O2 -g -Wall -Wwrite-strings -Wcast-align -Wstrict-aliasing -Wstrict-prototypes -Wpointer-arith -Wundef -Wshadow -Wfloat-equal -Waggregate-return -Wcast-qual -Wswitch-default -Wswitch-enum -Wunreachable-code -Wformat=2 -std=gnu11 -D_FILE_OFFSET_BITS=64 -fstrict-aliasing -pipe -Wextra -Wstrict-overflow=5 -Winit-self"

set -x

gcc $CFLAGS -c spotun.c -o spotun.o
gcc $CFLAGS spotun.o $LDFLAGS -o spotun
