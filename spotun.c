/* Simple Port Tunnel network utility
 * Copyright (C) 2023 by Jody Bruchon <jody@jodybruchon.com>
 * Licensed under The MIT License; see LICENSE.txt for details
 */

#if defined _WIN32 || defined __MINGW32__
 #ifndef ON_WINDOWS
  #define ON_WINDOWS 1
 #endif
 #ifndef WIN32_LEAN_AND_MEAN
  #define WIN32_LEAN_AND_MEAN
 #endif
 #include <windows.h>
 #include <winsock2.h>
 #include <ws2tcpip.h>
#else
 #include <sys/types.h>
 #include <sys/socket.h>
 #include <netinet/in.h>
 #include <arpa/inet.h>
 #include <netdb.h>
#endif /* Win32 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "version.h"


#ifndef RPTR_ADDR
 #define RPTR_ADDR "127.0.0.1"
#endif

char *program_name;

/* Replace these later */
int tunnel_port = 5678;
int repeat_port = 9154;


void clean_exit(int status)
{
#ifdef ON_WINDOWS
	WSACleanup();
#endif
	exit(status);
}


void helptext(int ret)
{
	fprintf(stderr, "usage:   %s repeater in_port proto:port [proto:port ...]\n", program_name);
	fprintf(stderr, "         %s receiver repeater_addr:rptr_in_port proto:port=addr:port\n\n", program_name);
	fprintf(stderr, "example: forward TCP 9154 from PC with public IP to server-pc behind CGNAT:\n");
	fprintf(stderr, "         %s repeater 5678 tcp:9154\n", program_name);
	fprintf(stderr, "         %s receiver hostname.com:5678 tcp:9154=server-pc:9154\n", program_name);

	switch (ret) {
		default:
		case -1:
			exit(EXIT_FAILURE);
		case 1:
			exit(EXIT_SUCCESS);
		case 0:
			return;
	}
}


int main_receiver(int argc, char **argv)
{
	struct sockaddr_in tunnel_sa_in;
	int tunnel_socket;
	int cnt;
	char buf[1024];

	if (argc < 3) goto error_args;
	// Connect to a repeater
	tunnel_sa_in.sin_family = AF_INET;
	tunnel_sa_in.sin_addr.s_addr = inet_addr(RPTR_ADDR);
	tunnel_sa_in.sin_port = htons(tunnel_port);
	tunnel_socket = socket(AF_INET, SOCK_STREAM, 0);

	errno = 0;
	if (connect(tunnel_socket, (struct sockaddr *)&tunnel_sa_in, sizeof(tunnel_sa_in)) == -1) goto error_connect;
	if (send(tunnel_socket, argv[2], strlen(argv[2]), 0) == -1) goto error_send;
	cnt = recv(tunnel_socket, buf, 1024, 0);

	if (cnt == 1024) cnt = 1023;
	buf[cnt] = '\0';
	printf("result: [%d] '%s' [%s]", cnt, buf, strerror(errno));

	close(tunnel_socket);

	// When repeater sends a connection, repeat it to configured clients
	return 0;

error_args:
	fprintf(stderr, "Not enough arguments specified\n");
	helptext(-1);
error_connect:
	fprintf(stderr, "Failed to connect: %s\n", strerror(errno));
	return -1;
error_send:
	fprintf(stderr, "Failed to send: %s\n", strerror(errno));
	return -2;
}


int main_repeater(int argc, char **argv)
{
	struct sockaddr_in tunnel_sa_in;
	struct sockaddr_in client_sa_in;
	int cnt;
	int client_sfd, connect_fd;
	char buf[1024];
	socklen_t len = sizeof(client_sa_in);

	if (argc < 3) goto error_args;
	// Listen on a port for connections from receiver
	tunnel_sa_in.sin_family = AF_INET;
	tunnel_sa_in.sin_addr.s_addr = htonl(INADDR_ANY);
	tunnel_sa_in.sin_port = htons(tunnel_port);

	errno = 0;
	client_sfd = socket(AF_INET, SOCK_STREAM, 0);
	if (client_sfd < 0) goto error_socket;
	if (bind(client_sfd, (struct sockaddr *)&tunnel_sa_in, sizeof(tunnel_sa_in)) != 0) goto error_bind;
	if (listen(client_sfd, 5) == -1) goto error_listen;
	connect_fd = accept(client_sfd, (struct sockaddr *)&tunnel_sa_in, &len);
	if (connect_fd == -1) goto error_accept;
	cnt = recv(connect_fd, buf, 1024, 0);
	if (cnt == 1024) cnt = 1023;
	buf[cnt] = '\0';
	printf("result: [%d] '%s' [%s]", cnt, buf, strerror(errno));
	cnt = send(connect_fd, argv[2], strlen(argv[2]), 0);
	if (cnt == -1) goto error_send;
	close(client_sfd);

	// Listen on "in" port(s) to repeat to receiver

	// When a connection arrives on an in port, repeat it to receiver

	return 0;

error_args:
	fprintf(stderr, "Not enough arguments specified\n");
	helptext(-1);
error_socket:
	fprintf(stderr, "Failed to open socket: %s\n", strerror(errno));
	return -1;
error_bind:
	fprintf(stderr, "Failed to bind port: %s\n", strerror(errno));
	return -2;
error_listen:
	fprintf(stderr, "Failed to listen: %s\n", strerror(errno));
	return -3;
error_accept:
	fprintf(stderr, "Failed to accept: %s\n", strerror(errno));
	return -4;
error_send:
	fprintf(stderr, "Failed to send: %s\n", strerror(errno));
	return -5;
}


int main(int argc, char **argv)
{
#ifdef ON_WINDOWS
	WSADATA wsaData;
	int ws_result;
#endif

	fprintf(stderr, "Simple Port Tunneling utility %s (%s)\n", VER, VERDATE);
	program_name = argv[0];

#ifdef ON_WINDOWS
	ws_result = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (ws_result != 0) {
		fprintf(stderr, "WSAStartup failed: %d\n", ws_result);
		clean_exit(EXIT_FAILURE);
	}
#endif

	// do code stuff
	if (argc < 2) helptext(-1);
	if (strcmp(argv[1], "repeater") == 0) main_repeater(argc, argv);
	else if (strcmp(argv[1], "receiver") == 0) main_receiver(argc, argv);
	else helptext(-1);

	clean_exit(EXIT_SUCCESS);
}
