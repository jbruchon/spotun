/* VERSION determines the program's version number
 * This file is part of jdupes; see jdupes.c for license information */

#ifndef SPOTUN_VERSION_H
#define SPOTUN_VERSION_H

#define VER "0.1"
#define VERDATE "2023-10-26"

#endif /* SPOTUN_VERSION_H */
