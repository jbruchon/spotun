Introduction
-------------------------------------------------------------------------------
Jody Bruchon's Simple Port Tunneler: a tool to forward a public IP port through
a NAT without a VPN or SSH.

This program is very incomplete so documentation here is mainly for Jody's own
benefit.


Repeater packet format
-------------------------------------------------------------------------------
The first byte is always a type byte with the following types:
0x00: reserved, discard
0x01: ping: reply to sender with a pong
0x02: pong: reply for a ping
0x03: start a new connection
0x04: traffic for an existing connection
0x05: terminate an existing connection
0x06: terminate all connections (reset)
0x07-0xff: ignore

The second and third byte are always a 16-bit length for the rest of the data
that is expected to arrive.

Repeater connection setup:
1. Repeater receives a TCP connection on a monitored port
2. Repeater sends 0x03 packet to receiver with port number as a u16
3. Receiver replies with 0x03 packet with connection ID# as a u16
4. Repeater/receiver exchange 0x04 packets with ID# as first u16

Connection termination packets only have the conn ID# u16 inside.


TODO list
-------------------------------------------------------------------------------
Connections should have some basic security to ensure that random traffic is
not being sent and accepted.


Contact information
-------------------------------------------------------------------------------
Bug reports/feature requests: https://codeberg.org/jbruchon/spotun/issues
For all other spotun inquiries, contact Jody Bruchon <jody@jodybruchon.com>


Legal information and software license
-------------------------------------------------------------------------------
spotun is Copyright (C) 2023 by Jody Bruchon <jody@jodybruchon.com>

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
